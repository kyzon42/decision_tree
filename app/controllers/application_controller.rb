class ApplicationController < ActionController::API
  around_action :handle_exceptions

  def catch_404
    raise ActionController::RoutingError.new("Page not found: /#{params[:path]}")
  end

  private

  def handle_exceptions
    begin
      yield
    rescue ActionController::RoutingError, ActiveRecord::RecordNotFound => e
      @status = 404
      @message = e.message
    rescue ActionController::ParameterMissing, ArgumentError => e
      @status = 400
    rescue StandardError => e
      @status = 500
    end
    render json: { error: e.message, status: @status }.to_json, status: @status if e.class != NilClass
  end
end
