class DecisionTreesController < ApplicationController
  # GET /decision_tree
  def show
    result = DecisionTree::Builder.new.call

    render json: result
  end
end
