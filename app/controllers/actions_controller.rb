class ActionsController < ApplicationController
  before_action :find_action, only: %i[show destroy]
  before_action :find_all_actions, only: %i[index]

  # GET /actions
  def index
    render json: @actions
  end

  # POST /actions
  def create
    action = Action.new(properties: properties_params)
    raise ArgumentError, action.errors.messages unless action.save

    render json: { status: 201 }, status: 201
  end

  # GET /actions/:id
  def show
    render json: @action.to_json(except: %i[created_at updated_at])
  end

  # DELETE /actions/:id
  def destroy
    @action.destroy
    render json: { status: 200 }, status: 200
  end

  private

  def find_action
    @action = Action.find(params[:id])
  end

  def find_all_actions
    @actions = Action.all
  end

  def properties_params
    params.require(:properties).permit!
  end
end
