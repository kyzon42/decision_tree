module Objects
  class ActionsController < ApplicationController
    # GET /objects/actions
    def index
      check_keys_and_values(object_params)

      action_ids = DecisionTree::Search.new.call(object: object_params)
      render json: find_actions(action_ids)
    end

    private

    def object_params
      params.require(:object).permit!.to_h
    end

    def find_actions(ids)
      Action.where(id: ids).select(:id, :properties).map(&:attributes)
    end

    # TODO: remove duplication with Action#keys_and_values_are_string
    def check_keys_and_values(properties)
      return if properties.is_a?(Hash) && (properties.keys + properties.values).all? { |el| el.is_a? String }

      raise ArgumentError, 'Allowed only properties with format { String => String }'
    end
  end
end
