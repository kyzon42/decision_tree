# == Schema Information
#
# Table name: actions
#
#  id         :bigint(8)        not null, primary key
#  properties :jsonb
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Action < ApplicationRecord
  PROPERTIES_MAX_SIZE = 10

  validates :properties, presence: true
  validate :properties_limit, :keys_and_values_are_string

  private

  def properties_limit
    return if properties.nil? || properties.size <= PROPERTIES_MAX_SIZE
    errors.add(
      :properties, "Allowed only properties with size <= #{PROPERTIES_MAX_SIZE}. Your size is #{properties.size}"
    )
  end

  def keys_and_values_are_string
    return if properties.nil? || (properties.keys + properties.values).all? { |el| el.is_a? String }
    errors.add(:properties, 'Allowed only properties with format { String => String }')
  end
end
