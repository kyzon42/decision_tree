module DecisionTree
  class Builder
    def call(actions: nil)
      actions ||= Action.select(:id, :properties).map(&:attributes)
      calc_decision_tree(actions: actions)
    end

    private

    def calc_decision_tree(actions:, default: [])
      return default unless actions.present?

      node = find_heaviest_node(actions)

      node_actions    = actions.select { |act| act['properties'].key? node }
      default_actions = actions.reject { |act| act['properties'].key? node }

      {
        'key'     => node,
        'values'  => find_values(node, node_actions),
        'default' => calc_decision_tree(actions: default_actions, default: default)
      }
    end

    #
    # For choosing the heaviest node we count properties of actions which it is included
    #
    def find_heaviest_node(actions)
      property_keys = actions.map { |act| act['properties'].keys }

      data = Hash.new(0)

      property_keys.each do |arr|
        arr.each do |el|
          data[el] += arr.size
        end
      end

      data.max_by { |_k, v| v }[0]
    end

    def find_values(node, node_actions)
      values = {}
      node_actions.each do |act|
        act = act.deep_dup
        val = act['properties'].delete(node)

        values[val] ||= { actions: [], default: [] }
        if act['properties'].present?
          values[val][:actions] << act
        else
          values[val][:default] << act['id']
        end
      end

      values.each do |val, attrs|
        values[val] = calc_decision_tree(attrs)
      end

      values
    end
  end
end
