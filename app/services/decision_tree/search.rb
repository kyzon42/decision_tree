module DecisionTree
  class Search
    def call(object:, decision_tree: nil)
      decision_tree ||= DecisionTree::Builder.new.call
      return decision_tree if decision_tree.blank?

      ids = search_by_properties(object: object, decision_tree: decision_tree)
      ids.uniq
    end

    private

    def search_by_properties(object:, decision_tree:)
      object.flat_map do |key, _value|
        check_node(decision_tree: decision_tree, object: object, property_key: key)
      end
    end

    def check_node(decision_tree:, object:, property_key: nil)
      condition_for_key_exsistence = if property_key
                                       property_key == decision_tree['key']
                                     else
                                       object.key? decision_tree['key']
                                     end

      sub_tree = if condition_for_key_exsistence
                   obj_value = object[decision_tree['key']]

                   decision_tree['values'][obj_value] || []
                 else
                   decision_tree['default']
                 end

      sub_tree.is_a?(Array) ? sub_tree : check_node(decision_tree: sub_tree, object: object)
    end
  end
end
