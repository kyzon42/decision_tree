Rails.application.routes.draw do
  root to: redirect('/actions')

  resources :actions, only: %i[index show create destroy]

  resource :decision_tree, only: :show

  namespace :objects do
    resources :actions, only: :index
  end

  match '*path', to: 'application#catch_404', via: :all
end
