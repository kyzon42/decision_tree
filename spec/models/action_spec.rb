# == Schema Information
#
# Table name: actions
#
#  id         :bigint(8)        not null, primary key
#  properties :jsonb
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Action, type: :model do
  describe 'Validations' do
    it { is_expected.to validate_presence_of(:properties) }

    context 'with custom validations' do
      let(:action) { build(:action, properties: invalid_properties) }
      subject { action.valid? }

      context '#properties_limit' do
        let(:invalid_properties) do
          invalid_size = described_class::PROPERTIES_MAX_SIZE + 1
          Array.new(invalid_size, 'foo').map.with_index { |el, i| [el + i.to_s, "bar_#{i}"] }.to_h
        end

        it { is_expected.to be_falsey }
      end

      context '#keys_and_values_are_string' do
        let(:invalid_properties) { { 'ok' => true } }

        it { is_expected.to be_falsey }
      end
    end
  end
end
