# == Schema Information
#
# Table name: actions
#
#  id         :bigint(8)        not null, primary key
#  properties :jsonb
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :action do
    properties do
      num_of_words = rand(1..Action::PROPERTIES_MAX_SIZE)

      Array.new(2, Faker::Lorem.words(num_of_words)).transpose.to_h
    end
  end
end
