require 'rails_helper'
require_relative 'shared_data'

RSpec.describe DecisionTree::Search do
  let(:custom_expected_decision_tree) { expected_decision_tree }
  subject { described_class.new.call(object: object, decision_tree: custom_expected_decision_tree) }

  describe 'basic example' do
    include_context 'shared data'

    context 'object without actions' do
      let(:object) do
        { 'real' => 'no', 'type' => 'cat', 'name' => 'Murka' }
      end

      it { is_expected.to eq [] }
    end

    context 'object with action' do
      let(:object) do
        { 'color' => 'green', 'location' => 'unknown', 'type' => 'martian', 'weight' => 'light' }
      end

      it { is_expected.to eq [action_1.id] }
    end

    context 'object with 2 actions' do
      let(:object) do
        { 'color' => 'red', 'location' => 'Moscow', 'real' => 'no' }
      end

      it { expect(subject.sort).to eq [action_2.id, action_3.id].sort }
    end

    context 'edge case (default -> deafult) action' do
      let(:object) do
        { 'color' => 'red', 'country' => 'Russia', 'real' => 'no' }
      end
      let!(:action_4) { create :action, properties: { 'country' => 'Russia' } }

      let(:custom_expected_decision_tree) do
        expected_decision_tree['default']['default'] = {
          'key' => 'country',
          'values' => {
            'Russia' => [action_4.id]
          },
          'default' => []
        }
        expected_decision_tree
      end

      it { expect(subject.sort).to eq [action_2.id, action_4.id].sort }
    end
  end
end
