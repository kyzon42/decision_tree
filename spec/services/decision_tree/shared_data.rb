RSpec.shared_context 'shared data' do
  let!(:action_1) { create(:action, properties: { color: 'green', location: 'unknown' }) }
  let!(:action_2) { create(:action, properties: { color: 'red', real: 'no' }) }
  let!(:action_3) { create(:action, properties: { location: 'Moscow' }) }

  let(:expected_decision_tree) do
    {
      'key' => 'color',
      'values' => {
        'green' => {
          'key' => 'location',
          'values' => {
            'unknown' => [action_1.id]
          },
          'default' => []
        },
        'red' => {
          'key' => 'real',
          'values' => {
            'no' => [action_2.id]
          },
          'default' => []
        }
      },
      'default' => {
        'key' => 'location',
        'values' => {
          'Moscow' => [action_3.id]
        },
        'default' => []
      }
    }
  end
end
