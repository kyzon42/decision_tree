require 'rails_helper'
require_relative 'shared_data'

RSpec.describe DecisionTree::Builder do
  subject { described_class.new.call }

  describe 'basic example' do
    include_context 'shared data'

    it { is_expected.to eq expected_decision_tree }

    context 'actions with equal properties' do
      let!(:action_4) { create(:action, properties: { color: 'red', real: 'no' }) }

      it { expect(subject['values']['red']['values']['no']).to eq [action_2.id, action_4.id] }
    end

    context 'with sub-action' do
      let!(:action_5) { create(:action, properties: { color: 'green' }) }

      it { expect(subject['values']['green']['default']).to eq [action_5.id] }
    end
  end

  context 'without actions' do
    it { is_expected.to eq [] }
  end
end
