require 'rails_helper'

RSpec.describe ActionsController, type: :request do
  describe 'GET /actions' do
    let!(:actions) { create_list(:action, 3) }

    context 'with valid request' do
      before do
        get '/actions'
      end

      it { expect(json.count).to eq 3 }
      it { expect(response).to have_http_status(:ok) }
    end
  end

  describe 'POST /actions' do
    before do
      post '/actions', params: my_params
    end

    context 'with valid request' do
      let(:my_params) { { properties: { color: 'red' } } }

      it { expect(response).to have_http_status(:created) }
      it { expect(Action.count).to eq 1 }
    end

    context 'with invalid request' do
      let(:my_params) do
        invalid_properties = Array.new(Action::PROPERTIES_MAX_SIZE + 1) do |i|
          ["ok#{i}", 'da']
        end.to_h

        { properties: invalid_properties }
      end

      it { expect(response).to have_http_status(:bad_request) }
      it { expect(json['status']).to eq 400 }
    end
  end

  describe 'GET /actions/:id' do
    before do
      get "/actions/#{action_id}"
    end

    context 'with valid request' do
      let(:action) { create(:action) }
      let(:action_id) { action.id }

      it { expect(response).to have_http_status(:ok) }
      it { expect(json.keys.sort).to eq %w[id properties].sort }
    end

    context 'with invalid request' do
      let(:action_id) { 42 }

      it { expect(response).to have_http_status(:not_found) }
      it { expect(json['status']).to eq 404 }
    end
  end

  describe 'DELETE /actions/:id' do
    before do
      delete "/actions/#{action_id}"
    end

    context 'with valid request' do
      let!(:action) { create(:action) }
      let(:action_id) { action.id }

      it { expect(response).to have_http_status(:ok) }
      it { expect(Action.count).to eq 0 }
    end

    context 'with invalid request' do
      let(:action_id) { 42 }

      it { expect(response).to have_http_status(:not_found) }
      it { expect(json['status']).to eq 404 }
    end
  end
end
