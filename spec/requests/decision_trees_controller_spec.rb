require 'rails_helper'
require_relative '../services/decision_tree/shared_data'

RSpec.describe DecisionTreesController, type: :request do
  describe 'GET /decision_tree' do
    include_context 'shared data'

    context 'with valid request' do
      before do
        get '/decision_tree'
      end

      it { expect(json).to eq expected_decision_tree }
      it { expect(response).to have_http_status(:ok) }
    end
  end
end
