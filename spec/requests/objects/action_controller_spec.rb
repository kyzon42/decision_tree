require 'rails_helper'

RSpec.describe Objects::ActionsController, type: :request do
  describe 'GET /objects/actions' do
    before do
      get '/objects/actions', params: my_params
    end

    context 'with valid request' do
      let(:my_params) { { object: { color: 'red' } } }

      it { expect(response).to have_http_status(:ok) }
    end

    context 'with invalid request' do
      let(:my_params) { { object: { color: ['red', 'green'] } } }

      it { expect(response).to have_http_status(:bad_request) }
      it { expect(json['status']).to eq 400 }
    end
  end
end
