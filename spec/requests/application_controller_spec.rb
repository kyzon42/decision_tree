require 'rails_helper'

RSpec.describe ApplicationController, type: :request do
  describe 'exception handler' do
    context 'with routing error' do
      before do
        get '/actio'
      end

      it { expect(response).to have_http_status(:not_found) }
    end

    context 'with any argument error' do
      before do
        get '/objects/actions'
      end

      it { expect(response).to have_http_status(:bad_request) }
    end

    context 'with exception' do
      before do
        allow(DecisionTree::Builder).to receive(:new).and_raise(StandardError)
        get '/decision_tree'
      end

      it { expect(response).to have_http_status(:internal_server_error)}
    end
  end
end
